from urllib.parse import urljoin

from appstraction import App
from router import Router

from . import pages


class NewAppRouter(Router):
    def __init__(self, app):
        super().__init__()
        self.app = app
        for page_name, page_cls in self.app.registry.items:
            self.add(page_name, page_cls.path, getattr(page_cls, 'fragment', None))

    def generate(self, name, query=None, fragment=None, **variables):
        path = super().generate(name, query=query, fragment=fragment, **variables)
        return urljoin(self.app.base_url, path)


def build_app():
    class Application(App):
        def __init__(self, driver, base_url):
            super().__init__(driver, base_url)
            self.router = NewAppRouter(app=self)

    Application.register_pages(
        *pages.articles.pages,
        *pages.auth.pages,
        *pages.feed.pages,
    )

    return Application
