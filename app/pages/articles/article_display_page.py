from appstraction.page import Page


class ArticleDisplayPage(Page):
    name = 'Article:Display'
    path = r'^/$'
    fragment = r'^/article/{slug:[\w\-]+}-{id:\w{6}}$'

    @property
    def title(self):
        selector = "css=.article-page .container > h1"
        return self.driver.find_element_by(selector)

    @property
    def meta(self):
        selector = "xpath=//node()[contains(@class, 'article-meta')]"
        return self.driver.find_element_by(selector)

    @property
    def meta_edit_button(self):
        selector = "xpath=.//a[contains(text(), 'Edit Article')]"
        return self.meta.find_element_by(selector)

    @property
    def meta_delete_button(self):
        selector = "xpath=.//button[contains(text(), 'Delete Article')]"
        return self.meta.find_element_by(selector)

    @property
    def meta_author(self):
        selector = "xpath=.//a[contains(@class, 'author')]"
        return self.meta.find_element_by(selector)

    @property
    def meta_date(self):
        selector = "xpath=.//span[contains(@class, 'date')]"
        return self.meta.find_element_by(selector)

    @property
    def content(self):
        selector = "xpath=//node()[contains(@class, 'article-content')]"
        return self.driver.find_element_by(selector)
