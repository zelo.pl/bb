from appstraction.page import Page


class ArticleUpdatePage(Page):
    name = 'Article:Update'
    path = r'^/$'
    fragment = r'^/editor/{slug:[\w\-]+}-{id:\w{6}}$'
