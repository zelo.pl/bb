from appstraction.page import Page


class ArticleCreatePage(Page):
    name = 'Article:Create'
    path = r'^/$'
    fragment = r'^/editor$'

    def create(self, title=None, description=None, content=None, tags=None, submit=False):
        if title is not None:
            selector = 'xpath=//input[@placeholder="Article Title"]'
            self.driver.find_element_by(selector).write(title)
        if description is not None:
            selector = 'xpath=//input[@placeholder="What\'s this article about?"]'
            self.driver.find_element_by(selector).write(title)
        if content is not None:
            selector = 'xpath=//textarea[@placeholder="Write your article (in markdown)"]'
            self.driver.find_element_by(selector).write(content)
        if tags is not None:
            selector = 'xpath=//input[@placeholder="Enter Tags"]'
            self.driver.find_element_by(selector).write(tags)
        if submit is not None:
            selector = 'xpath=//button[contains(text(), "Publish Article")]'
            self.driver.find_element_by(selector).write(title)
