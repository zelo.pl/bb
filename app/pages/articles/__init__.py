from .article_create_page import ArticleCreatePage
from .article_display_page import ArticleDisplayPage
from .article_update_page import ArticleUpdatePage


pages = (ArticleCreatePage, ArticleDisplayPage, ArticleUpdatePage)
