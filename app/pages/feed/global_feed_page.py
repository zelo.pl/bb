from appstraction.page import Page


class GlobalFeedPage(Page):
    name = 'GlobalFeed'
    path = r'^/$'
    fragment = r'^/$'
