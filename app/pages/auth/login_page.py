from appstraction.page import Page


class AuthLoginPage(Page):
    name = 'Auth:Login'
    path = r'^/$'
    fragment = r'^/login$'

    def login(self, username=None, password=None, submit=False):
        if username is not None:
            selector = 'xpath=//input[@placeholder="Username"]'
            self.driver.find_element_by(selector).write(username)
        if password is not None:
            selector = 'xpath=//input[@placeholder="Password"]'
            self.driver.find_element_by(selector).write(password)
        if submit is True:
            selector = 'xpath=//button[contains(text(), "Sign in")]'
            self.driver.find_element_by(selector).click()
