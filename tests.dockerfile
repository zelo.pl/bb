FROM python:3.8

COPY ./requirements.txt /cwd/requirements.txt
RUN pip install -r /cwd/requirements.txt

RUN wget https://repo.maven.apache.org/maven2/io/qameta/allure/allure-commandline/2.13.4/allure-commandline-2.13.4.tgz -O allure.tgz \
 && tar zxvf allure.tgz \
 && rm allure.tgz \
 && mv allure-2.13.4 /opt/allure
ENV PATH=/opt/allure/bin:$PATH

RUN apt-get update \
 && apt-get -y install openjdk-11-jre-headless

EXPOSE 8000
WORKDIR /cwd
CMD PYTHONPATH=/cwd pytest --alluredir=./allure; allure generate --clean ./allure && cd ./allure-report && python3 -m http.server