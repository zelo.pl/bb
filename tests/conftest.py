import pytest
from driver import Remote
from selenium.webdriver import DesiredCapabilities

from app.app import build_app


@pytest.fixture(scope='session')
def browser():
    # quick hack, should be handled by querying hub api
    # from time import sleep
    # sleep(10)
    driver = Remote(command_executor='http://selenium-hub:4444/wd/hub', desired_capabilities=DesiredCapabilities.CHROME)
    yield driver
    driver.quit()


@pytest.fixture()
def driver(browser):
    browser.delete_all_cookies()
    yield browser


@pytest.fixture()
def base_url():
    return "https://candidatex:qa-is-cool@qa-task.backbasecloud.com/"


@pytest.fixture()
def app(base_url, driver):
    return build_app()(base_url=base_url, driver=driver)


@pytest.fixture()
def user():
    return type('User', (),
                {'username': 'ffd1c7e3d530432c8597605e0a3abfe', 'email': 'ffd1c7e3d530432c8597605e0a3abfe@example.com',
                 'password': 'password'})
