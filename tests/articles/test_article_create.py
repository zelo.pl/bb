from datetime import datetime

import allure


@allure.title('Article CRUD: Open article creation page as unauthorized user')
def test_article_create_as_unauthorized_used(app):
    allure.step('Given I\'m not logged in')
    with allure.step('When I open article creation page'):
        app.get_page('Article:Create')
    with allure.step('Then I\'m redirected to login page'):
        app.wait_until_current_page_is('Auth:Login', timeout=1)


@allure.title('Article CRUD: Open article creation page as authorized user')
def test_article_create_as_authorized_used(app, user):
    with allure.step(f'Given I\'m authorized as {user.email}'):
        page = app.get_page('Auth:Login')
        page.login(username=user.email, password=user.password, submit=True)
        app.wait_until_current_page_is('GlobalFeed')
    with allure.step('When I open article creation page'):
        page = app.get_page('Article:Create')
    with allure.step('And I fill and submit article creation form'):
        art_title = 'Test title'
        art_description = 'Test description'
        art_content = "Test Content"
        art_tags = 'one, two'
        page.create(
            title=art_title,
            description=art_description,
            content=art_content,
            tags=art_tags,
            submit=True
        )
    with allure.step('Then I\'m redirected to article details page'):
        app.wait_until_current_page_is('Article:Display')
        page = app.get_page('Article:Display', navigate=False)
    with allure.step(f'And displayed article title is "{art_title}"'):
        assert page.title.text == art_title
    with allure.step(f'And displayed article author is {user.username}'):
        assert page.meta_author.text == user.username
    art_date = f'{datetime.now():%B %-d, %Y}'
    with allure.step(f'And displayed meta date is {art_date}'):
        assert page.meta_date.text == art_date
    with allure.step(f'And displayed content is {art_content}'):
        assert page.content.text == art_content
