Tests
=====

# Run
```bash
git clone git@gitlab.com:zelo.pl/bb.git \
&& cd bb \
&& docker-compose pull \
&& docker-compose build tests \
&& docker-compose up -d selenium-hub selenium-chrome \
&& sleep 5 \
&& docker-compose up tests \
; docker-compose down
```

# Report
Report will be served on `localhost:8000` after this log line `Report successfully generated to allure-report`
